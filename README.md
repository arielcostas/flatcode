# FlatCode Theme

A port of the FlatLaf theme available in Apache NetBeans, for VSCodium (and VSCode).

This theme is still in development, and is [available in Open-VSX](https://open-vsx.org/extension/arielcostas/flatcode), meaning you can download it directly from [VSCodium](https://vscodium.com/). For users of the proprietary VSCode, it can also be downloaded from Open-VSX as a `.vsix` file you can then install.
